package merr

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitee.com/yunind/mistful/mlog"
)

var (
	// ErrInputIsEmpty indicates the input was required but empty
	ErrInputIsEmpty error = errors.New("empty")
	// ErrPkNotEmpty indicates the pk is not empty
	// use when add new entity
	ErrPkNotEmpty error = &SInputError{map[string]string{"pk": "is not empty"}}
	// ErrPkEmpty indicates the pk is empty
	// use when update or delete
	ErrPkEmpty error = &SInputError{map[string]string{"pk": "is empty"}}
	// ErrDataNotFound indicates the request value is not found
	ErrDataNotFound error = &SInputError{map[string]string{"data": "not found"}}
)

// SInputError representing input errors
type SInputError struct {
	ErrorMap map[string]string
}

func (err SInputError) Error() string {
	var mss []string
	for p, m := range err.ErrorMap {
		mss = append(mss, p+" "+m)
	}
	return "input:" + strings.Join(mss, ",")
}

func (err SInputError) Status() int {
	return http.StatusBadRequest
}

// Err returns an immutable error respresenting s;
func (err *SInputError) Err() *RPCStatus {
	return &RPCStatus{Code: InvalidArgument, Message: err.Error()}
}

// SInputItemError representing input errors of one item
type SInputItemError struct {
	Message string
	Value   interface{}
}

func (err SInputItemError) Error() string {
	return err.Message
}

// InputC collects input errors with name
// nil return when no errors
func InputC(em map[string]interface{}) error {
	errMap := make(map[string]string)
	desc := "输入判断"
	for n, i := range em {
		if i != nil {
			if n == "" {
				panic("input error name can not be empty")
			}
			if e, ok := i.(error); ok {
				if ie, ok := e.(*SInputItemError); ok {
					mlog.WriteMsg(mlog.LErr, fmt.Sprintf(mlog.GetDesFormat(desc), desc, fmt.Sprintf("input:%s %s value %v", n, ie.Message, ie.Value)), 2)
					errMap[n] = e.Error()
				} else {
					errMap[n] = e.Error()
				}

			} else if s, ok := i.(string); ok {
				errMap[n] = s
			}
		}
	}
	if len(errMap) == 0 {
		return nil
	}
	return &SInputError{ErrorMap: errMap}
}
