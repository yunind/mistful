package merr

import "net/http"

var (
	// ErrAuthUserNotFound indicates user is not found when login
	ErrAuthUserNotFound error = &SAuthError{Message: "login user is not found"}
	// ErrAuthPasswordIncrypt indicates password is incrypt
	ErrAuthPasswordIncrypt error = &SAuthError{Message: "password is incrypt"}
	// ErrAuthNot indicates unauthentication
	ErrAuthNot error = &SAuthError{Message: "unauthentication"}
)

// SAuthError representing auth errors
type SAuthError struct {
	Message string
}

func (err SAuthError) Error() string {
	return "auth:" + err.Message
}

func (err SAuthError) Status() int {
	return http.StatusUnauthorized
}

// Err returns an immutable error representing s
func (err *SAuthError) Err() *RPCStatus {
	return &RPCStatus{Code: Unauthenticated, Message: err.Error()}
}
