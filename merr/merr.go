// Package merr make easy to handle error for mist service
/*
Implements errors returned by gRPC
*/
package merr

import (
	"fmt"
)

// Merr defines mist error interface
type Merr interface {
	Err() *RPCStatus
	Status() int
}

// RPCStatus represents an  status code, message, and details.  It is immutable
// and should be created with New, Newf, or FromProto.
type RPCStatus struct {
	Code    Code
	Message string
	Details []interface{}
}

// Err returns an immutable error representing s; returns nil if s.Code() is OK.
func (s *RPCStatus) Err() error {
	if s.Code == OK {
		return nil
	}
	return &Error{s: s}
}

// Error wraps a pointer of a status proto. It implements error and Status,
// and a nil *Error should never be returned by this package.
type Error struct {
	s *RPCStatus
}

func (e *Error) Error() string {
	return fmt.Sprintf("mist error: code = %s desc = %s", e.s.Code, e.s.Message)
}
