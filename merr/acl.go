package merr

import "net/http"

// SACLError representing access control errors
type SACLError struct {
	EspectRole string
}

func (err SACLError) Error() string {
	if err.EspectRole == "" {
		return "acl: permission deny"
	}
	return "acl: espect role " + err.EspectRole
}

func (err SACLError) Status() int {
	return http.StatusForbidden
}

// Err returns an immutable error representing s
func (err *SACLError) Err() *RPCStatus {
	return &RPCStatus{Code: PermissionDenied, Message: err.Error()}
}

// ACLC collects access control errors
// espect role
func ACLC(role string) error {
	return &SACLError{EspectRole: role}
}
