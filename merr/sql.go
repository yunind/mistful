package merr

import (
	"fmt"
	"net/http"

	"gitee.com/yunind/mistful/mlog"
)

// SSQLError representing sql query error
type SSQLError struct {
	Message string
}

func (err SSQLError) Error() string {
	return "sql:" + err.Message
}

func (err SSQLError) Status() int {
	return http.StatusInternalServerError
}

// Err implement Merr interface
func (err SSQLError) Err() *RPCStatus {
	return &RPCStatus{Code: Internal, Message: "database error"}
}

// SQLC collects sql query error
func SQLC(err error) error {
	if err == nil {
		return nil
	}
	desc := "数据库"
	mlog.WriteMsg(mlog.LErr, fmt.Sprintf(mlog.GetDesFormat(desc), desc, err.Error()), 2)
	return &SSQLError{}
}
