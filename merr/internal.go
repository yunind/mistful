package merr

import (
	"fmt"

	"gitee.com/yunind/mistful/mlog"
)

// SInternalError representing internal errors
type SInternalError struct {
	Message string
}

func (err SInternalError) Error() string {
	return "internal:" + err.Message
}

// Err implement Merr interface
func (err SInternalError) Err() *RPCStatus {
	return &RPCStatus{Code: Internal, Message: "unknown error"}
}

// InternalC collects source error
// source name called
func InternalC(sn string, err error, opt ...interface{}) error {
	if err == nil {
		return nil
	}
	desc := "资源"
	deepOffset := 0
	if len(opt) > 0 {
		switch val := opt[0].(type) {
		case int:
			deepOffset += val
		}
	}
	mlog.WriteMsg(mlog.LErr, fmt.Sprintf(mlog.GetDesFormat(desc), desc, err.Error()), 2+deepOffset)
	return SInternalError{Message: sn}
}
