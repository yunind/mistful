package streamlib

import "google.golang.org/grpc"

// ClientStream for mist
type ClientStream interface {
	RecvModel() (interface{}, error)
	SendModelAndClose(interface{}) error
	grpc.ServerStream
}
