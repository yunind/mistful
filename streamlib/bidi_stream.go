package streamlib

import "google.golang.org/grpc"

// BidiStream for mist
type BidiStream interface {
	RecvModel() (interface{}, error)
	SendModel(model interface{}) error
	grpc.ServerStream
}
