package streamlib

import (
	"google.golang.org/grpc"
)

// ServerStream for mist
type ServerStream interface {
	SendModel(model interface{}) error
	grpc.ServerStream
}
