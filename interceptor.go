package mistful

import (
	"context"

	"google.golang.org/grpc"
)

// ServerInfo represents server information
type ServerInfo struct {
	MistName    string
	ServiceName string
}

// UnaryServerInfo consists of various information about a mist server
type UnaryServerInfo struct {
	ServerInfo
	ModelName string
}

// StreamServerInfo consists of various information about a mist server on a stream
type StreamServerInfo struct {
	*ServerInfo
	IsClientStream bool
	IsServerStream bool
}

// UnaryServerInterceptor provides a hook to intercept the execution of a unary
type UnaryServerInterceptor func(ctx context.Context, req interface{}, info *UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error)

// StreamServerInterceptor provides a hook to intercept the execution of a streaming RPC on the server.
type StreamServerInterceptor func(srv interface{}, ss grpc.ServerStream, info *StreamServerInfo, handler grpc.StreamHandler) error

// MistServerStream is a thin wrapper around grpc.ServerStream that allows modifying context.
type MistServerStream struct {
	grpc.ServerStream
	// MistContext is own context of mist service
	MistContext context.Context
}

// Context returns the mist server's MistContext, overwriting the nested grpc.ServerStream.Context()
func (m *MistServerStream) Context() context.Context {
	return m.MistContext
}

// WrapServerStream returns a ServerStream that has the ability to overwrite context.
func WrapServerStream(stream grpc.ServerStream) *MistServerStream {
	if exiting, ok := stream.(*MistServerStream); ok {
		return exiting
	}
	return &MistServerStream{ServerStream: stream, MistContext: stream.Context()}
}
