package middleware

// Option of middleware
type Option interface {
	// middleware's name
	Name() string
}

// ManagerOption takes config option for manager
type ManagerOption interface {
	apply(op *managerOptions)
}

type managerOptions struct {
	middlewareOptions map[string][]Option
}

type funcManagerOption struct {
	f func(*managerOptions)
}

func (fdo *funcManagerOption) apply(do *managerOptions) {
	fdo.f(do)
}

func newFuncManagerOption(f func(*managerOptions)) *funcManagerOption {
	return &funcManagerOption{
		f: f,
	}
}

// DeployMiddleware deploy configuration of middleware
func DeployMiddleware(middles ...Option) ManagerOption {
	return newFuncManagerOption(func(o *managerOptions) {
		for _, op := range middles {
			name := op.Name()
			o.middlewareOptions[name] = append(o.middlewareOptions[name], op)
		}
	})
}
