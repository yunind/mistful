package middleware

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
)

type configFile struct {
	name     string
	services []configService
}

type configService struct {
	name string
	path string
}

// ReadConfig read all configuration of middlewares
func (m *Manager) ReadConfig(path string) error {

	// travales directory of configuration path
	fs := make(map[string]*configFile)
	rd, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	for _, r := range rd {
		ns := strings.Split(r.Name(), ".")
		if len(ns) != 2 {
			continue
		}
		cname := ns[0]
		ext := ns[1]
		if ext == "yaml" || ext == "yml" {
			ssplit := strings.Split(cname, "_")
			if len(ssplit) == 1 {
				// mismatch service configuration
				cf := &configFile{
					name: cname,
					services: []configService{{
						name: "",
						path: filepath.Join(path, r.Name()),
					}},
				}
				fs[cname] = cf
				continue
			}
			// match service configuration
			sname := ssplit[1]
			if cf := fs[cname]; cf != nil {
				cf.services = append(cf.services, configService{
					name: sname,
					path: filepath.Join(path, r.Name()),
				})
			} else {
				cf := &configFile{
					name: cname,
					services: []configService{
						{
							name: sname,
							path: filepath.Join(path, r.Name()),
						},
					},
				}
				fs[cname] = cf
			}
		}
	}
	// call middleware ReadYAML function
	m.mwMu.RLock()
	defer m.mwMu.RUnlock()
	for n, middle := range m.mws {
		if app, ok := middle.(ReadYAML); ok {
			// check if has server config file
			cf := fs[n]
			if cf == nil {
				continue
			}
			for _, s := range cf.services {
				buf, err := ioutil.ReadFile(s.path)
				if err != nil {
					return fmt.Errorf("read %s of %s configuration error:%s", s.name, n, err.Error())
				}
				err = app.ReadYAML(s.name, buf)
				if err != nil {
					return fmt.Errorf("read %s of %s configuration error:%s", s.name, n, err.Error())
				}
			}
		}
	}
	return nil
}
