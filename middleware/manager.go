package middleware

import (
	"sync"

	"gitee.com/yunind/mistful"
)

var (
	inter mistful.UnaryServerInterceptor
)

func init() {
	defaultManager.mws = make(map[string]Middleware)
	defaultManager.opt = &managerOptions{
		middlewareOptions: make(map[string][]Option),
	}

}

// Manager manage middlewares
type Manager struct {
	mwMu       sync.RWMutex
	mws        map[string]Middleware
	priorities priorities
	opt        *managerOptions
}

// DefaultManager is the default manager
var DefaultManager = &defaultManager

var defaultManager Manager

func GetUnaryServerInterceptor() mistful.UnaryServerInterceptor {
	if inter == nil {
		inter = mistful.ChainUnaryServer(defaultManager.UnaryInterceptors()...)
	}
	return inter
}
