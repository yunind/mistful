package middleware

import "gitee.com/yunind/mistful/installer"

// InstallerWrapper is the interface that can be implemented by a middleware
// It wrappe context base on service name or default service name
type InstallerWrapper interface {
	// Wrappers returns installer wrappers
	Wrappers() []installer.Wrapper
}

// RegisterWrappers returns installer wrappers of middleware
func (m *Manager) RegisterWrappers() {
	m.mwMu.RLock()
	defer m.mwMu.RUnlock()
	for n, mw := range m.mws {
		if iw, ok := mw.(InstallerWrapper); ok {
			for _, w := range iw.Wrappers() {
				installer.RegisterWrapper(n, w)
			}
		}
	}
}
