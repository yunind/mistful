package middleware

import (
	"sort"

	"gitee.com/yunind/mistful"
)

// Middleware defines middlewares for mist server
type Middleware interface {
	Init(ops ...Option) error
	UnaryInterceptors() []mistful.UnaryServerInterceptor
	StreamInterceptors() []mistful.StreamServerInterceptor
}

// ReadYAML read yaml file for unmarshal configuration
type ReadYAML interface {
	ReadYAML(string, []byte) error
}

// Register middleware with manager
// 1 ~ 5 for config
// 6 ~ 10 for database
// 11 ~ 20 for authentication
// 21 ~ 30 for access control
// 31 ~ 40 for mist link
func Register(name string, middleware Middleware, pri int) {
	defaultManager.mwMu.Lock()
	defer defaultManager.mwMu.Unlock()
	if middleware == nil {
		panic("middleware: Register middleware is nil")
	}
	if _, duplicated := defaultManager.mws[name]; duplicated {
		panic("middleware: Register call twice for middleware " + name)
	}
	defaultManager.mws[name] = middleware
	defaultManager.priorities = append(defaultManager.priorities, priority{p: pri, n: name})
}

// Init initial manager
// sort with priority
func (m *Manager) Init(opts ...ManagerOption) error {
	m.mwMu.RLock()
	defer m.mwMu.RUnlock()
	for _, o := range opts {
		o.apply(m.opt)
	}
	for mn, middle := range m.mws {
		opts := m.opt.middlewareOptions[mn]
		err := middle.Init(opts...)
		if err != nil {
			return err
		}
	}
	sort.Sort(m.priorities)
	return nil
}

// UnaryInterceptors returns interceptors of all middleware
func (m *Manager) UnaryInterceptors() []mistful.UnaryServerInterceptor {
	m.mwMu.RLock()
	defer m.mwMu.RUnlock()

	var uis []mistful.UnaryServerInterceptor
	for _, p := range m.priorities {
		middle := m.mws[p.n]
		uis = append(uis, middle.UnaryInterceptors()...)
	}
	return uis
}

// StreamInterceptors returns interceptors of all middleware
func (m *Manager) StreamInterceptors() []mistful.StreamServerInterceptor {
	m.mwMu.RLock()
	defer m.mwMu.RUnlock()
	var sis []mistful.StreamServerInterceptor
	for _, p := range m.priorities {
		middle := m.mws[p.n]
		sis = append(sis, middle.StreamInterceptors()...)
	}
	return sis
}
