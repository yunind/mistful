package middleware

// Priority level for named middleware
type priority struct {
	p int
	n string
}

type priorities []priority

func (p priorities) Len() int {
	return len(p)
}

func (p priorities) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p priorities) Less(i, j int) bool {
	return p[i].p < p[j].p
}
