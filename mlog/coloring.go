package mlog

// ConsoleType the color type
type ConsoleType int

const (
	_ ConsoleType = iota + 29
	// ConsoleGray print with gray color
	ConsoleGray
	// ConsoleRed print with red color
	ConsoleRed
	// ConsoleGreen print with green color
	ConsoleGreen
	// ConsoleYellow print with yellow color
	ConsoleYellow
	// ConsoleBlue print with blue color
	ConsoleBlue
)
