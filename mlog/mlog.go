// Package mlog log for mist
package mlog

import (
	"fmt"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
	"unicode"
)

// LogLevel is enum type of log level
type LogLevel int

const (
	_ LogLevel = iota
	// LDebug debug level
	LDebug
	// LWarn warn level
	LWarn
	// LErr error level
	LErr
)

func format(f interface{}, v ...interface{}) string {
	var msg string
	switch f.(type) {
	case string:
		msg = f.(string)
		if len(v) == 0 {
			return msg
		}
		if !strings.Contains(msg, "%") {
			msg += strings.Repeat(" %v", len(v))
		}
	default:
		msg = fmt.Sprint(f)
		if len(v) == 0 {
			return msg
		}
		msg += strings.Repeat(" %v", len(v))
	}
	if len(v) > 0 {
		return fmt.Sprintf(msg, v...)
	}
	return fmt.Sprint(msg)
}

// Debugd format print debug info with description
func Debugd(desc string, f interface{}, v ...interface{}) {
	WriteMsg(LDebug, fmt.Sprintf(GetDesFormat(desc), desc, format(f, v...)), 2)
}

// Warnd format print warning with description
func Warnd(desc string, f interface{}, v ...interface{}) {
	WriteMsg(LWarn, fmt.Sprintf(GetDesFormat(desc), desc, format(f, v...)), 2)
}

// Errord format print error info with description
func Errord(desc string, f interface{}, v ...interface{}) {
	WriteMsg(LErr, fmt.Sprintf(GetDesFormat(desc), desc, format(f, v...)), 2)
}

// WriteMsg write message to log writer
func WriteMsg(l LogLevel, msg string, dep int) {
	_, file, line, _ := runtime.Caller(dep)
	os.Stdout.Write(append([]byte(fmt.Sprintf("\x1b[1;%dm%s:%d\x1b[0m", ConsoleGray, file, line)), '\n'))
	ts := time.Now().Format("2006-01-02 15:04:05")
	switch l {
	case LDebug:
		msg = fmt.Sprintf("%s \x1b[0;%dm[调试]\x1b[0m %s", ts, ConsoleGreen, msg)
	case LWarn:
		msg = fmt.Sprintf("%s \x1b[0;%dm[警告]\x1b[0m %s", ts, ConsoleYellow, msg)
	case LErr:
		msg = fmt.Sprintf("%s \x1b[0;%dm[错误]\x1b[0m %s", ts, ConsoleRed, msg)
	}
	os.Stdout.Write(append([]byte(msg), '\n'))
}

// GetDesFormat return format string for description
func GetDesFormat(d string) string {
	han := 0
	for _, r := range d {
		if unicode.Is(unicode.Scripts["Han"], r) {
			han++
		}
	}
	if han > 5 {
		return "%6s %s"
	}
	return "%" + strconv.Itoa(12-han) + "s %s"
}
