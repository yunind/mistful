package installer

import "context"

// Install function with wrapped context
type Install func(context.Context) error
