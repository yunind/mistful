// Package installer install some information or create database tables
package installer

import (
	"context"
	"sync"
)

var (
	installersMu sync.RWMutex
	installers   []Installer
)

// Installer install information for mist project
type Installer struct {
	// ServiceName for middleware to design which data to be wrapped
	ServiceName string
	installs    []Install
}

// NewInstaller create and register mist installer
func NewInstaller(name string, installs []Install) {
	installersMu.Lock()
	defer installersMu.Unlock()
	if installs == nil {
		panic("installer: New installers's install is nil")
	}
	installers = append(installers, Installer{name, installs})
}

// Run this installer
func Run() error {
	installersMu.RLock()
	defer installersMu.RUnlock()
	wrappersMu.RLock()
	defer wrappersMu.RUnlock()
	for _, ins := range installers {
		beWrapCtx := context.Background()
		var err error
		for _, wrapper := range wrappers {
			beWrapCtx, err = wrapper(beWrapCtx, ins.ServiceName)
			if err != nil {
				return err
			}
		}
		for _, install := range ins.installs {
			// TODO: debug print
			err = install(beWrapCtx)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
