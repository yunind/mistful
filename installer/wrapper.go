package installer

import (
	"context"
	"sync"
)

var (
	wrappersMu sync.RWMutex
	wrappers   = make(map[string]Wrapper)
)

// Wrapper wrap context before call install
type Wrapper func(ctx context.Context, serviceName string) (context.Context, error)

// RegisterWrapper add wrapper for installer
func RegisterWrapper(name string, wrapper Wrapper) {
	wrappersMu.Lock()
	defer wrappersMu.Unlock()
	if wrapper == nil {
		panic("installer: Register wrapper is nil")
	}
	if _, duplicated := wrappers[name]; duplicated {
		panic("installer: Register call twice for wrapper " + name)
	}
	wrappers[name] = wrapper
}
