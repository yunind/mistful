package mistful

type RestResponse struct {
	Result interface{} `json:"result"`
	Model  interface{} `json:"model"`
	Err    string      `json:"error"`
}
