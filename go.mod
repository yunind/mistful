module gitee.com/yunind/mistful

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	google.golang.org/genproto v0.0.0-20210406143921-e86de6bf7a46 // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	xorm.io/builder v0.3.9 // indirect
	xorm.io/core v0.7.3 // indirect
)
