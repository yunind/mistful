package mistful

import (
	"errors"
	"net/http"
	"strings"
)

// IsGrpcRequest check if is grpc request
func IsGrpcRequest(r *http.Request) bool {
	return r.ProtoMajor == 2 && strings.HasPrefix(r.Header.Get("Content-Type"), "application/grpc")
}

// GetServiceName convert grpc sever full method string to serviceName
func GetServiceName(fullMethod string) (string, string, string, error) {
	dspli := strings.Split(fullMethod, ".")
	if len(dspli) < 3 {
		// mlog.Warnd("中间件", "illegal full method: miss mist name or service name")
		return "", "", "", errors.New("illegal full method")
	}
	mistName := dspli[0]
	if len(mistName) < 1 {
		// mlog.Warnd("中间件", "illegal full method: mist name empty")
		return "", "", "", errors.New("illegal full method")
	}
	mistName = mistName[1:]
	serviceName := dspli[1]
	valid, model := matchToFindModelName(mistName, serviceName, fullMethod)
	if !valid {
		// mlog.Warnd("中间件", "illegal full method: prefix %s check error", fullMethod)
		return "", "", "", errors.New("illegal full method")
	}
	return mistName, serviceName, model, nil
}

// GetFullMethod concat to  grpc server path
func GetFullMethod(mistName, serviceName, model string) string {
	if model == "" {
		model = "Miss"
	}
	return "/" + mistName + "." + serviceName + "." + strings.ToLower(serviceName[0:1]) + serviceName[1:] + "Server/Call" + model
}

// check if match mist project full path and return model
func matchToFindModelName(mistName, serviceName, fullMethod string) (bool, string) {
	up := upFirst(serviceName)
	m := "/" + mistName + "." + serviceName + "." + up + "Server/Call"
	model := strings.TrimPrefix(fullMethod, m)

	return model != fullMethod, model
}

func upFirst(serviceName string) string {
	return strings.ToUpper(serviceName[0:1]) + serviceName[1:]
}

// SnakeCasedName convert name to snake cased name
func SnakeCasedName(name string) string {
	newstr := make([]rune, 0)
	for idx, chr := range name {
		if isUpper := 'A' <= chr && chr <= 'Z'; isUpper {
			if idx > 0 {
				newstr = append(newstr, '_')
			}
			chr -= ('A' - 'a')
		}
		newstr = append(newstr, chr)
	}

	return string(newstr)
}
