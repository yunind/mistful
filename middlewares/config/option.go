package config

import "gitee.com/yunind/mistful/middleware"

// Option for middleware config
type Option struct {
	ids     []string
	root    string
	addr    string
	withTLS bool
}

// Name implement middleware option
func (o Option) Name() string {
	return middleName
}

// DataStore let service can store file data
func DataStore(ids ...string) middleware.Option {
	return Option{
		ids: ids,
	}
}

// DataRoot set data root
// this will override prefore when call twice
func DataRoot(root string) middleware.Option {
	return Option{
		root: root,
	}
}

// DataAddr set address
func DataAddr(addr string, withTLS bool) middleware.Option {
	return Option{
		addr:    addr,
		withTLS: withTLS,
	}
}
