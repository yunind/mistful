package config

import "gitee.com/yunind/mistful/installer"

// Wrappers implement InstallerWrapper
func (middle *Config) Wrappers() []installer.Wrapper {
	return []installer.Wrapper{
		wrapContext,
	}
}
