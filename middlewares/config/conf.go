package config

import (
	"context"
	"path/filepath"
)

// GetConf returns configuration
func GetConf(ctx context.Context) *ConfData {
	di := ctx.Value(Config{})
	if di == nil {
		panic("not wrap with middleware config data")
	}
	return di.(*ConfData)
}

// GetDefaultData returns default configuration data
func GetDefaultData() map[string]interface{} {
	return datas["default"]
}

// GetPaths returns all server virtual path
func GetPaths() map[string]string {
	snm := make(map[string]string, len(ids))
	for _, id := range ids {

		snm[id] = filepath.Join(root, "data", id)
	}
	return snm
}

// GetServiceNames returns all service names that setting on config
func GetServiceNames() []string {
	var sns []string
	for _, id := range ids {
		sns = append(sns, id)
	}
	return sns
}

// GetAddr returns tcp address conf
func GetAddr() *Addr {
	r := new(Addr)
	*r = addr
	return r
}
