package config

import (
	"gopkg.in/yaml.v3"
)

var (
	// root path of current project
	root  string
	ids   []string
	datas      = make(map[string]map[string]interface{})
	addr  Addr = Addr{host: "localhost", port: 8080}
)

// Config store mist base config data
type Config struct{}

// ConfData representing configuration data
type ConfData struct {
	ServiceName string
	Path        string // server's data path that separate from each service
	Data        map[string]interface{}
}

// ReadYAML read yaml data implement middleware
func (middle *Config) ReadYAML(serviceName string, dbuf []byte) error {
	data := make(map[string]interface{})
	err := yaml.Unmarshal(dbuf, &data)
	if err != nil {
		return err
	}
	if serviceName == "" {
		datas["default"] = data
		return nil
	}

	if datas["default"] == nil {
		datas["default"] = data
	}
	datas[serviceName] = data
	return nil
}
