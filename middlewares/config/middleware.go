// Package config middleware for mist project
// 可以在中间件配置入口配置开启缓存的服务
// 可以配置文件形式配置，将被入口配置覆盖
package config

import (
	"context"
	"errors"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitee.com/yunind/mistful"
	"gitee.com/yunind/mistful/middleware"
	"google.golang.org/grpc"
)

func init() {
	middleware.Register("config", &Config{}, 1)
}

var (
	middleName  = "config"
	initialized = false
)

func wrapContext(ctx context.Context, serviceName string) (context.Context, error) {
	var data map[string]interface{}
	if d, had := datas[serviceName]; had {
		data = d
	} else if d, had := datas["default"]; had {
		data = d
	} else {
		// TODO: warning
	}
	cd := &ConfData{
		ServiceName: serviceName,
		Path:        filepath.Join(root, "data", serviceName),
		Data:        data,
	}
	newCtx := context.WithValue(ctx, Config{}, cd)
	return newCtx, nil
}

// Init implement middleware
func (middle *Config) Init(opts ...middleware.Option) error {
	if initialized {
		panic("call config init after initialized")
	}
	// 从配置文件中读取
	if datas["default"] != nil && datas["default"]["config"] != nil {
		configInter := datas["default"]["config"]
		if configs, ok := configInter.(map[string]interface{}); ok {
			if configs["services"] != nil {
				if idsi, ok := configs["services"].([]interface{}); ok {
					for _, idi := range idsi {
						if id, ok := idi.(string); ok {
							ids = append(ids, id)
						}
					}
				} else {
					return errors.New("config -> services is not string array on yaml file")
				}
			}

			if configs["addr"] != nil {
				if addrC, ok := configs["addr"].(map[string]interface{}); ok {
					if addrC["host"] != nil {
						if host, ok := addrC["host"].(string); ok {
							addr.host = host
						} else {
							return errors.New("config -> addr -> host is not string on yaml file")
						}
					}
					if addrC["port"] != nil {
						if port, ok := addrC["port"].(int); ok {
							addr.port = port
						} else {
							return errors.New("config -> addr -> port is not int on yaml file")
						}
					}
					// TODO: tls with tls file
				} else {
					return errors.New("config -> addr is not map on yaml file")
				}
			}
		}
	}
	// 输入需要建立文件数据的服务配置信息
	om := make(map[string]bool)
	// 监听地址
	addrConf := ""
	withTLSConf := false
	for _, opi := range opts {
		op := opi.(Option)
		for _, id := range op.ids {
			om[id] = true
		}
		if op.root != "" {
			root = op.root
		}
		if op.addr != "" {
			addrConf = op.addr
			withTLSConf = op.withTLS
		}
	}
	for id := range om {
		ids = append(ids, id)
	}
	if root == "" {
		root, _ = os.Getwd()
	}
	err := os.Mkdir(filepath.Join(root, "data"), os.ModePerm)
	if err != nil {
		if !os.IsExist(err) {
			return err
		}
	}
	for _, id := range ids {
		err := os.Mkdir(filepath.Join(root, "data", id), os.ModePerm)
		if !os.IsExist(err) {
			return err
		}
	}
	// 监听地址解析
	if addrConf != "" {
		p := strings.Split(addrConf, ":")
		if len(p) == 1 {
			return errors.New("address is invalid")
		}
		if p[0] != "" {
			addr.host = p[0]
		}
		if p[1] != "" {
			port, err := strconv.Atoi(p[1])
			if err != nil {
				return err
			}
			addr.port = port
		}
		addr.withTLS = withTLSConf
	}
	initialized = true
	return nil
}

// UnaryInterceptors implement middleware
func (middle *Config) UnaryInterceptors() []mistful.UnaryServerInterceptor {
	return []mistful.UnaryServerInterceptor{
		func(ctx context.Context, req interface{}, info *mistful.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
			newCtx, err := wrapContext(ctx, info.ServiceName)
			if err == nil {
				return handler(newCtx, req)
			}
			return nil, err
		},
	}
}

// StreamInterceptors implement middleware
func (middle *Config) StreamInterceptors() []mistful.StreamServerInterceptor {
	return []mistful.StreamServerInterceptor{
		func(srv interface{}, ss grpc.ServerStream, info *mistful.StreamServerInfo, handler grpc.StreamHandler) error {
			mistSS := mistful.WrapServerStream(ss)
			var err error
			mistSS.MistContext, err = wrapContext(mistSS.Context(), info.ServiceName)
			if err == nil {
				return handler(srv, mistSS)
			}
			return nil
		},
	}
}
