package config

import (
	"net"
	"strconv"
)

// Addr help to get some information about current server
type Addr struct {
	host    string
	port    int
	withTLS bool
}

// GetURL returns servers url
func (a *Addr) GetURL() string {
	var proto string
	if a.withTLS {
		proto = "https://"
	} else {
		proto = "http://"
	}
	return proto + net.JoinHostPort(a.host, strconv.Itoa(addr.port))
}

// Port return port for server
func (a *Addr) Port() string {
	return ":" + strconv.Itoa(addr.port)
}
