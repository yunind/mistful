// Package mkv middleware for msit project
// support for key value setting on context
// feature
// 1. with etcd
package mkv

import (
	"context"

	"gitee.com/yunind/mistful"
	"gitee.com/yunind/mistful/middleware"
	"google.golang.org/grpc"
)

var (
	initialized = false
)

func init() {
	middleware.Register("mkv", &Mkv{}, 50)
}

func warpContext(ctx context.Context) (context.Context, error) {
	kv := &KV{
		values: make(map[string]interface{}),
	}
	newCtx := context.WithValue(ctx, Mkv{}, kv)
	return newCtx, nil
}

// Init implement middleware
func (middle *Mkv) Init(opts ...middleware.Option) error {
	if initialized {
		panic("call mkv init after initialized")
	}
	return nil
}

// UnaryInterceptors implement middleware
func (middle *Mkv) UnaryInterceptors() []mistful.UnaryServerInterceptor {
	return []mistful.UnaryServerInterceptor{
		func(ctx context.Context, req interface{}, info *mistful.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
			var err error
			ctx, err = warpContext(ctx)
			if err == nil {
				return handler(ctx, req)
			}
			return nil, err
		},
	}
}

// StreamInterceptors implement middleware
func (middle *Mkv) StreamInterceptors() []mistful.StreamServerInterceptor {
	return []mistful.StreamServerInterceptor{
		func(srv interface{}, ss grpc.ServerStream, info *mistful.StreamServerInfo, handler grpc.StreamHandler) error {
			mistSS := mistful.WrapServerStream(ss)
			var err error
			mistSS.MistContext, err = warpContext(mistSS.Context())
			if err == nil {
				return handler(srv, mistSS)
			}
			return nil
		},
	}
}
