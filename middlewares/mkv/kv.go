package mkv

// KV support saving value by key on context
type KV struct {
	values map[string]interface{}
}

// SetModel cache model to context
func (kv *KV) SetModel(key string, model interface{}) {
	kv.values[key] = model
}

// GetModel return cached model data
func (kv *KV) GetModel(key string) interface{} {
	return kv.values[key]
}
