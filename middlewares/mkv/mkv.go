package mkv

import "context"

// Mkv mist key value cache
type Mkv struct {
}

// Of return kv
func Of(ctx context.Context) *KV {
	ki := ctx.Value(Mkv{})
	kv, ok := ki.(*KV)
	if !ok {
		panic("not wrap with middleware mkv")
	}
	return kv
}
