package mistful

type Mist struct {
	Params     map[string]interface{} `json:"params"`
	MethodName string                 `json:"methodName"`
}
